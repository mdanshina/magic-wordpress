#!/bin/bash

DIR=$(dirname $(readlink -f $0))

sudo /bin/bash -c "
        ln -sf ${DIR}/sites/000-default.conf /etc/apache2/sites-enabled/

        mkdir -p /etc/service/apache
        cp ${DIR}/service/run /etc/service/apache/
        chmod +x /etc/service/apache/run

        sv restart apache > /dev/null
"
